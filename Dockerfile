FROM alpine:3.8

ENV DB_DATA_PATH="/var/lib/mysql" \
    DB_ROOT_PASS="root" \
    DB_NAME="" \
    DB_USER="" \
    DB_PASS="" \
    MAX_ALLOWED_PACKET="200M"

RUN apk update && \
    apk add --no-cache mariadb mariadb-client pwgen && \
    rm -f /var/cache/apk/*

ADD run.sh /run.sh

RUN chmod 755 run.sh

#VOLUME ["/var/lib/mysql"]

EXPOSE 3306

ENTRYPOINT ["/run.sh"]