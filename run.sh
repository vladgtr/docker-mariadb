#!/bin/sh

echo '* Working around permission errors locally by making sure that "mysql" uses the same uid and gid as the host volume'
TARGET_UID=$(stat -c "%u" /var/lib/mysql)
echo '-- Setting mysql user to use uid '$TARGET_UID
usermod -o -u $TARGET_UID mysql || true
TARGET_GID=$(stat -c "%g" /var/lib/mysql)
echo '-- Setting mysql group to use gid '$TARGET_GID
groupmod -o -g $TARGET_GID mysql || true

if [ -d "/run/mysqld" ]; then
	echo "[i] mysqld already present, skipping creation"
else
	echo "[i] mysqld not found, creating...."
	/usr/bin/mysqld_safe --user=mysql --datadir=$DB_DATA_PATH --bootstrap --verbose=0
fi

chown -R mysql:mysql /run/mysqld

if [ -d /var/lib/mysql/mysql ]; then
	echo "[i] MySQL directory already present, skipping creation"
	chown -R mysql:mysql /var/lib/mysql
else
	echo "[i] MySQL data directory not found, creating initial DBs"

	chown -R mysql:mysql /var/lib/mysql

	mysql_install_db --user=mysql --datadir=$DB_DATA_PATH > /dev/null

	if [ "$DB_ROOT_PASS" = "" ]; then
		DB_ROOT_PASS=`pwgen 16 1`
		echo "[i] MySQL root Password: $DB_ROOT_PASS"
	fi

	DB_NAME=${DB_NAME:-""}
	DB_USER=${DB_USER:-""}
	DB_PASS=${DB_PASS:-""}

	tfile=`mktemp`
	if [ ! -f "$tfile" ]; then
	    return 1
	fi

	cat << EOF > $tfile
USE mysql;
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' identified by '$DB_ROOT_PASS' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' identified by '$DB_ROOT_PASS' WITH GRANT OPTION;
UPDATE user SET password=PASSWORD('$DB_ROOT_PASS') WHERE user='root';
DROP DATABASE test;
EOF

	if [ "$DB_NAME" != "" ]; then
	    echo "[i] Creating database: $DB_NAME"
	    echo "CREATE DATABASE IF NOT EXISTS \`$DB_NAME\` CHARACTER SET utf8 COLLATE utf8_general_ci;" >> $tfile

	    if [ "$DB_USER" != "" ]; then
	        if [ "$DB_PASS" = "" ]; then
	            DB_PASS=`pwgen 16 1`
	        fi
		echo "[i] Creating user: $DB_USER with password $DB_PASS"
		echo "GRANT ALL ON \`$DB_NAME\`.* to '$DB_USER'@'%' IDENTIFIED BY '$DB_PASS';" >> $tfile
	    fi
	fi

	/usr/bin/mysqld --user=mysql --datadir=$DB_DATA_PATH --bootstrap --verbose=0 < $tfile
	rm -f $tfile
fi

exec /usr/bin/mysqld --user=mysql --datadir=$DB_DATA_PATH --console